package by.itstep.j2019trainermanagerlapinskaya.service;

import by.itstep.j2019trainermanagerlapinskaya.dto.comment.CommentCreateDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.comment.CommentFullDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.comment.CommentPreviewDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.comment.CommentUpdateDto;

import java.util.List;

public interface CommentService {
    //1. Найти всё
    List<CommentPreviewDto> findAll();
    //2. Найти по ID
    CommentFullDto findById(Long id);
    //3. Сохранить новый
    CommentFullDto create(CommentCreateDto createDto);
    //4. Обновить существующий
    CommentFullDto update(CommentUpdateDto comment);
    //5. Удалить по ID
    void deleteById(Long id);
}
