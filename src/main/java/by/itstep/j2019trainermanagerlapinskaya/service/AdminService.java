package by.itstep.j2019trainermanagerlapinskaya.service;

import by.itstep.j2019trainermanagerlapinskaya.dto.admin.AdminCreateDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.admin.AdminFullDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.admin.AdminPreviewDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.admin.AdminUpdateDto;

import java.util.List;

public interface AdminService {

    //1. Найти всё
    List<AdminPreviewDto> findAll();
    //2. Найти по ID
    AdminFullDto findById(Long id);
    //3. Сохранить новый
    AdminFullDto create(AdminCreateDto createDto);
    //4. Обновить существующий
    AdminFullDto update(AdminUpdateDto admin);
    //5. Удалить по ID
    void deleteById(Long id);
}
