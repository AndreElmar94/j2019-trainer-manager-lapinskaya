package by.itstep.j2019trainermanagerlapinskaya.service;

import by.itstep.j2019trainermanagerlapinskaya.dto.trainer.TrainerCreateDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.trainer.TrainerFullDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.trainer.TrainerPreviewDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.trainer.TrainerUpdateDto;

import java.util.List;

public interface TrainerService {

    //1. Найти всё
    List<TrainerPreviewDto> findAll();
    //2. Найти по ID
    TrainerFullDto findById(Long id);
    //3. Сохранить новый
    TrainerFullDto create(TrainerCreateDto createDto);
    //4. Обновить существующий
    TrainerFullDto update(TrainerUpdateDto trainer);
    //5. Удалить по ID
    void deleteById(Long id);

}
