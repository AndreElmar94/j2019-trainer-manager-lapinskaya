package by.itstep.j2019trainermanagerlapinskaya.service;

import by.itstep.j2019trainermanagerlapinskaya.dto.appointment.AppointmentCreateDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.appointment.AppointmentFullDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.appointment.AppointmentPreviewDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.appointment.AppointmentUpdateDto;

import java.util.List;

public interface AppointmentService {

    //1. Найти всё
    List<AppointmentPreviewDto> findAll();
    //2. Найти по ID
    AppointmentFullDto findById(Long id);
    //3. Сохранить новый
    AppointmentFullDto create(AppointmentCreateDto createDto);
    //4. Обновить существующий
    AppointmentFullDto update(AppointmentUpdateDto appointment);
    //5. Удалить по ID
    void deleteById(Long id);
}
