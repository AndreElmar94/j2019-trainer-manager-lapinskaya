package by.itstep.j2019trainermanagerlapinskaya.service.impl;
import by.itstep.j2019trainermanagerlapinskaya.dto.appointment.AppointmentCreateDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.appointment.AppointmentFullDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.appointment.AppointmentPreviewDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.appointment.AppointmentUpdateDto;
import by.itstep.j2019trainermanagerlapinskaya.entity.Appointment;
import by.itstep.j2019trainermanagerlapinskaya.mapper.AppointmentMapper;
import by.itstep.j2019trainermanagerlapinskaya.repository.AppointmentRepository;
import by.itstep.j2019trainermanagerlapinskaya.repository.impl.AppointmentRepositoryImpl;
import by.itstep.j2019trainermanagerlapinskaya.service.AppointmentService;

import java.util.List;

public class AppointmentServiceImpl implements AppointmentService {

    private AppointmentRepository appointmentRepository = new AppointmentRepositoryImpl();
    private AppointmentMapper mapper = new AppointmentMapper();

    @Override
    public List<AppointmentPreviewDto> findAll() {
        List<AppointmentPreviewDto> dtos = mapper.mapToDtoList(appointmentRepository.findAll());

        System.out.println("AppointmentServiceImpl -> found " + dtos.size() + " appointments");

        return dtos;
    }

    @Override
    public AppointmentFullDto findById(Long id) {
        AppointmentFullDto dto = mapper.mapToDto(appointmentRepository.findById(id));
        System.out.println("AppointmentServiceImpl -> found appointment" + dto);
        return dto;
    }

    @Override
    public AppointmentFullDto create(final AppointmentCreateDto createDto) {
//        Appointment appointment = appointmentRepository.findById(createDto.get());
        Appointment toSave = mapper.mapToEntity(createDto);

        final Appointment created = appointmentRepository.create(toSave);

        AppointmentFullDto createdDto = mapper.mapToDto(created);
        System.out.println("AppointmentItemServiceImpl -> create appointment " + created);
        return createdDto;
    }

    @Override
    public AppointmentFullDto update(final AppointmentUpdateDto updateDto) {
        Appointment toUpdate = mapper.mapToEntity(updateDto);
        Appointment existingEntity = appointmentRepository.findById(updateDto.getId());

        toUpdate.setArrivalTime(existingEntity.getArrivalTime());

        final Appointment updated = appointmentRepository.update(toUpdate);
        AppointmentFullDto updatedDto = mapper.mapToDto(updated);

        System.out.println("AppointmentServiceImpl -> update appointment " + updated);
        return updatedDto;

    }

    @Override
    public void deleteById(Long id) {
        appointmentRepository.deleteById(id);
        System.out.println("AppointmentServiceImpl -> appointment with id " + id + " was deleted");

    }
}
