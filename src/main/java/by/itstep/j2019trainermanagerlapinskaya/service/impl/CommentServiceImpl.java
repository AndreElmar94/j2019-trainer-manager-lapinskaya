package by.itstep.j2019trainermanagerlapinskaya.service.impl;

import by.itstep.j2019trainermanagerlapinskaya.dto.comment.CommentCreateDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.comment.CommentFullDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.comment.CommentPreviewDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.comment.CommentUpdateDto;
import by.itstep.j2019trainermanagerlapinskaya.entity.Appointment;
import by.itstep.j2019trainermanagerlapinskaya.entity.Comment;
import by.itstep.j2019trainermanagerlapinskaya.entity.Trainer;
import by.itstep.j2019trainermanagerlapinskaya.mapper.CommentMapper;
import by.itstep.j2019trainermanagerlapinskaya.repository.AppointmentRepository;
import by.itstep.j2019trainermanagerlapinskaya.repository.CommentRepository;
import by.itstep.j2019trainermanagerlapinskaya.repository.TrainerRepository;
import by.itstep.j2019trainermanagerlapinskaya.repository.impl.AppointmentRepositoryImpl;
import by.itstep.j2019trainermanagerlapinskaya.repository.impl.CommentRepositoryImpl;
import by.itstep.j2019trainermanagerlapinskaya.repository.impl.TrainerRepositoryImpl;
import by.itstep.j2019trainermanagerlapinskaya.service.CommentService;

import java.util.List;

public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository = new CommentRepositoryImpl();
    private final TrainerRepository trainerRepository = new TrainerRepositoryImpl();
    private final AppointmentRepository appointmentRepository = new AppointmentRepositoryImpl();
    private final CommentMapper commentMapper = new CommentMapper();

    @Override
    public List<CommentPreviewDto> findAll() {
        List<CommentPreviewDto> dtos = commentMapper.mapToDtoList(commentRepository.findAll());
        System.out.println("CommentServiceImpl -> found " + dtos.size() + " comments");
        return dtos;

    }

    @Override
    public CommentFullDto findById(Long id) {
        CommentFullDto dto = commentMapper.mapToDto(commentRepository.findById(id));
        System.out.println("CommentServiceImpl -> found comment" + dto);
        return dto;


    }

    @Override
    public CommentFullDto create(final CommentCreateDto createDto) {
        Trainer trainer = trainerRepository.findById(createDto.getTrainerId());
        Appointment appointment = appointmentRepository.findById(createDto.getUserId());

        Comment toSave = commentMapper.mapToEntity(createDto,trainer,appointment);
        Comment created = commentRepository.create(toSave);

        CommentFullDto dto = commentMapper.mapToDto(created);
        System.out.println("CommentServiceImpl -> create comment " + created);
        return dto;
    }

    @Override
    public CommentFullDto update(final CommentUpdateDto updateDto) {
        Comment toUpdate = commentMapper.mapToEntity(updateDto);
        Comment existingEntity = commentRepository.findById(updateDto.getId());
        toUpdate.setId(existingEntity.getId());
        toUpdate.setMessage(existingEntity.getMessage());

        Comment updated = commentRepository.update(toUpdate);

        CommentFullDto dto = commentMapper.mapToDto(updated);
        System.out.println("CommentServiceImpl -> create comment " + updated);
        return dto;
    }

    @Override
    public void deleteById(Long id) {
        commentRepository.deleteById(id);
        System.out.println("CommentServiceImpl -> comment with id " + id + " was deleted");

    }
}
