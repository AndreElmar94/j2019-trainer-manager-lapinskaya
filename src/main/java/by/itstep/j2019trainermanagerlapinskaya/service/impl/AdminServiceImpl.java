package by.itstep.j2019trainermanagerlapinskaya.service.impl;

import by.itstep.j2019trainermanagerlapinskaya.dto.admin.AdminCreateDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.admin.AdminFullDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.admin.AdminPreviewDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.admin.AdminUpdateDto;
import by.itstep.j2019trainermanagerlapinskaya.entity.Admin;
import by.itstep.j2019trainermanagerlapinskaya.mapper.AdminMapper;
import by.itstep.j2019trainermanagerlapinskaya.repository.AdminRepository;
import by.itstep.j2019trainermanagerlapinskaya.repository.impl.AdminRepositoryImpl;
import by.itstep.j2019trainermanagerlapinskaya.service.AdminService;

import java.util.List;

public class AdminServiceImpl implements AdminService {

    private final AdminRepository adminRepository = new AdminRepositoryImpl();
    private final AdminMapper mapper = new AdminMapper();

    @Override
    public List<AdminPreviewDto> findAll() {
        List<AdminPreviewDto> dtos = mapper.mapToDtoList(adminRepository.findAll());

        System.out.println("AdminRepositoryImpl -> found " + dtos.size() + " admins");

        return dtos;
    }

    @Override
    public AdminFullDto findById(final Long id) {
        AdminFullDto dto = mapper.mapToDto(adminRepository.findById(id));
        System.out.println("AdminRepositoryImpl -> found admin" + dto);
        return dto;
    }

    @Override
    public AdminFullDto create(final AdminCreateDto createDto) {
        Admin toSave = mapper.mapToEntity(createDto);

        final Admin created = adminRepository.create(toSave);
        AdminFullDto createdDto = mapper.mapToDto(created);
        System.out.println("AdminRepositoryImpl -> created admin" + created);
        return createdDto;
    }

    @Override
    public AdminFullDto update(final AdminUpdateDto updateDto) {
        Admin toUpdate = mapper.mapToEntity(updateDto);
        Admin existingEntity = adminRepository.findById(updateDto.getId());

        toUpdate.setName(existingEntity.getName());

        final Admin updated = adminRepository.update(toUpdate);
        AdminFullDto updatedDto = mapper.mapToDto(updated);

        System.out.println("AdminServiceImpl -> update admin " + updated);
        return updatedDto;
    }

    @Override
    public void deleteById(Long id) {
        adminRepository.deleteById(id);
        System.out.println("AdminRepositoryImpl -> admin with id " + id + " was deleted");

    }
}
