package by.itstep.j2019trainermanagerlapinskaya.service.impl;

import by.itstep.j2019trainermanagerlapinskaya.dto.trainer.TrainerCreateDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.trainer.TrainerFullDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.trainer.TrainerPreviewDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.trainer.TrainerUpdateDto;
import by.itstep.j2019trainermanagerlapinskaya.entity.Trainer;
import by.itstep.j2019trainermanagerlapinskaya.mapper.TrainerMapper;
import by.itstep.j2019trainermanagerlapinskaya.repository.TrainerRepository;
import by.itstep.j2019trainermanagerlapinskaya.repository.impl.TrainerRepositoryImpl;
import by.itstep.j2019trainermanagerlapinskaya.service.TrainerService;

import java.util.List;

public class TrainerServiceImpl implements TrainerService {
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();
    private TrainerMapper mapper = new TrainerMapper();

    @Override
    public List<TrainerPreviewDto> findAll() {
        List<TrainerPreviewDto> dtos = mapper.mapToDtoList(trainerRepository.findAll());
        System.out.println("TrainerServiceImpl -> found " + dtos.size() + " trainers");
        return dtos;
    }

    @Override
    public TrainerFullDto findById(Long id) {
        TrainerFullDto dto = mapper.mapToDto(trainerRepository.findById(id));
        System.out.println("TrainerServiceImpl -> found trainer" + dto);
        return dto;
    }

    @Override
    public TrainerFullDto create(final TrainerCreateDto createDto) {
//        Trainer trainer = trainerRepository.findById(createDto.get());
        Trainer toSave = mapper.mapToEntity(createDto);

        final Trainer created = trainerRepository.create(toSave);

        TrainerFullDto createdDto = mapper.mapToDto(created);
        System.out.println("TrainerServiceImpl -> create trainer " + created);
        return createdDto;
    }

    @Override
    public TrainerFullDto update(final TrainerUpdateDto updateDto) {
        Trainer toUpdate = mapper.mapToEntity(updateDto);
        Trainer existingEntity = trainerRepository.findById(updateDto.getId());

        toUpdate.setId(existingEntity.getId());

        final Trainer updated = trainerRepository.update(toUpdate);
        TrainerFullDto updatedDto = mapper.mapToDto(updated);

        System.out.println("TrainerServiceImpl -> update trainere " + updated);
        return updatedDto;
    }


    @Override
    public void deleteById(Long id) {
        trainerRepository.deleteById(id);
        System.out.println("TrainerServiceImpl -> trainer with id " + id + " was deleted");
    }
}
