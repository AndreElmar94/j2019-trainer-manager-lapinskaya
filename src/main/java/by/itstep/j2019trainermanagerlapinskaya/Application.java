package by.itstep.j2019trainermanagerlapinskaya;

import by.itstep.j2019trainermanagerlapinskaya.entity.Comment;
import by.itstep.j2019trainermanagerlapinskaya.util.EntityManagerUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);

		EntityManagerUtils.getEntityManager();
	}

}
