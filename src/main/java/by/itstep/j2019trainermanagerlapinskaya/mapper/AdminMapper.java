package by.itstep.j2019trainermanagerlapinskaya.mapper;
import by.itstep.j2019trainermanagerlapinskaya.dto.admin.AdminCreateDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.admin.AdminFullDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.admin.AdminPreviewDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.admin.AdminUpdateDto;
import by.itstep.j2019trainermanagerlapinskaya.entity.Admin;

import java.util.ArrayList;
import java.util.List;

public class AdminMapper {

    public List<AdminPreviewDto> mapToDtoList(List<Admin> entities) {
        List<AdminPreviewDto> dtos = new ArrayList<>();
        // 0. В цикле:
        for (Admin entity : entities) {
            // 1. Создать новый DTO
            AdminPreviewDto dto = new AdminPreviewDto();
            // 2. Перелить в него данные из ENTITY
            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setLastName(entity.getLastName());
            dto.setRole(entity.getRole());
            // 3. Доавить полученный DTO в список
            dtos.add(dto);
        }
        return dtos;
    }

    public Admin mapToEntity(AdminCreateDto createDto) {
        Admin admin = new Admin();

        admin.setName(createDto.getName());
        admin.setLastName(createDto.getLastName());
        admin.setPassword(createDto.getPassword());
        admin.setEmail(createDto.getEmail());

        return admin;
    }

    // updated_dto -> entity
    public Admin mapToEntity(AdminUpdateDto updateDto) {
        Admin entity = new Admin();
        entity.setId(updateDto.getId());
        entity.setEmail(updateDto.getEmail());
        entity.setName(updateDto.getName());
        entity.setLastName(updateDto.getLastName());
        entity.setPassword(updateDto.getPassword());

        return entity;
    }

    public AdminFullDto mapToDto(Admin entity) {

        AdminFullDto dto = new AdminFullDto();
        dto.setId(entity.getId());
        dto.setEmail(entity.getEmail());
        dto.setName(entity.getName());
        dto.setLastName(entity.getLastName());
        dto.setPassword(entity.getPassword());
        dto.setRole(entity.getRole());

        return dto;
    }
}
