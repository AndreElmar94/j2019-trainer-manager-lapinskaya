package by.itstep.j2019trainermanagerlapinskaya.mapper;
import by.itstep.j2019trainermanagerlapinskaya.dto.appointment.AppointmentCreateDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.appointment.AppointmentFullDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.appointment.AppointmentPreviewDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.appointment.AppointmentUpdateDto;
import by.itstep.j2019trainermanagerlapinskaya.entity.Appointment;
import java.util.ArrayList;
import java.util.List;

public class AppointmentMapper {

    public List<AppointmentPreviewDto> mapToDtoList(List<Appointment> entities) {
        List<AppointmentPreviewDto> dtos = new ArrayList<>();
        // 0. В цикле:
        for (Appointment entity : entities) {
            // 1. Создать новый DTO
            AppointmentPreviewDto dto = new AppointmentPreviewDto();
            // 2. Перелить в него данные из ENTITY
            dto.setId(entity.getId());
            dto.setClientName(entity.getClientName());
            dto.setClientLastName(entity.getClientLastName());
            dto.setClientEmail(entity.getClientEmail());
            dto.setNumberTel(entity.getNumberTel());
            // 3. Доавить полученный DTO в список
            dtos.add(dto);
        }
        return dtos;
    }

    public Appointment mapToEntity(AppointmentCreateDto createDto) {
        Appointment appointment = new Appointment();

        appointment.setClientName(createDto.getClientName());
        appointment.setClientLastName(createDto.getClientLastName());
        appointment.setClientEmail(createDto.getClientEmail());
        appointment.setArrivalTime(createDto.getArrivalTime());
        appointment.setNumberTel(createDto.getNumberTel());
        appointment.setTrainer(createDto.getTrainer());

        return appointment;
    }

    // updated_dto -> entity
    public Appointment mapToEntity(AppointmentUpdateDto updateDto) {
        Appointment entity = new Appointment();
        entity.setClientName(updateDto.getClientName());
        entity.setClientLastName(updateDto.getClientLastName());
        entity.setId(updateDto.getId());
        entity.setNumberTel(updateDto.getNumberTel());
        entity.setTrainer(updateDto.getTrainer());
        entity.setArrivalTime(updateDto.getArrivalTime());
        return entity;
    }

    public AppointmentFullDto mapToDto(Appointment entity) {

        AppointmentFullDto dto = new AppointmentFullDto();
        dto.setId(entity.getId());
        dto.setArrivalTime(entity.getArrivalTime());
        dto.setClientName(entity.getClientName());
        dto.setClientLastName(entity.getClientLastName());
        dto.setNumberTel(entity.getNumberTel());
        dto.setTrainer(entity.getTrainer());

        return dto;
    }

}
