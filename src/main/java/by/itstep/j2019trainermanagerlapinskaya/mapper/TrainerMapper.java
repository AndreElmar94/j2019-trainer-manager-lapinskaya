package by.itstep.j2019trainermanagerlapinskaya.mapper;
import by.itstep.j2019trainermanagerlapinskaya.dto.trainer.TrainerCreateDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.trainer.TrainerFullDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.trainer.TrainerPreviewDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.trainer.TrainerUpdateDto;
import by.itstep.j2019trainermanagerlapinskaya.entity.Trainer;

import java.util.ArrayList;
import java.util.List;

public class TrainerMapper {

    public List<TrainerPreviewDto> mapToDtoList(List<Trainer> entities) {
        List<TrainerPreviewDto> dtos = new ArrayList<>();
        // 0. В цикле:
        for (Trainer entity : entities) {
            // 1. Создать новый DTO
            TrainerPreviewDto dto = new TrainerPreviewDto();
            // 2. Перелить в него данные из ENTITY
            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setLastName(entity.getLastName());
            dto.setComments(entity.getComments());
            dto.setImageUrl(entity.getImageUrl());
            dto.setProgress(entity.getProgress());
            dto.setWorkExperience(entity.getWorkExperience());
            // 3. Доавить полученный DTO в список
            dtos.add(dto);
        }
        return dtos;
    }

    public Trainer mapToEntity(TrainerCreateDto createDto) {
        Trainer trainer = new Trainer();

        trainer.setName(createDto.getName());
        trainer.setLastName(createDto.getLastName());
        trainer.setPassword(createDto.getPassword());
        trainer.setEmail(createDto.getEmail());
        trainer.setImageUrl(createDto.getImageUrl());
        trainer.setProgress(createDto.getProgress());
        trainer.setWorkExperience(createDto.getWorkExperience());

        return trainer;
    }

    public Trainer mapToEntity(TrainerUpdateDto updateDto) {
        Trainer entity = new Trainer();
        entity.setId(updateDto.getId());
        entity.setEmail(updateDto.getEmail());
        entity.setName(updateDto.getName());
        entity.setLastName(updateDto.getLastName());
        entity.setPassword(updateDto.getPassword());
        entity.setWorkExperience(updateDto.getWorkExperience());
        entity.setProgress(updateDto.getProgress());
        entity.setImageUrl(updateDto.getImageUrl());

        return entity;
    }

    public TrainerFullDto mapToDto(Trainer entity) {

        TrainerFullDto dto = new TrainerFullDto();
        dto.setId(entity.getId());
        dto.setEmail(entity.getEmail());
        dto.setName(entity.getName());
        dto.setLastName(entity.getLastName());
        dto.setPassword(entity.getPassword());
        dto.setWorkExperience(entity.getWorkExperience());
        dto.setProgress(entity.getProgress());
        dto.setImageUrl(entity.getImageUrl());

        return dto;
    }
}
