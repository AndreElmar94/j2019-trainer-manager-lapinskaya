package by.itstep.j2019trainermanagerlapinskaya.mapper;
import by.itstep.j2019trainermanagerlapinskaya.dto.appointment.AppointmentCreateDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.comment.CommentFullDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.comment.CommentPreviewDto;
import by.itstep.j2019trainermanagerlapinskaya.dto.comment.CommentUpdateDto;
import by.itstep.j2019trainermanagerlapinskaya.entity.Appointment;
import by.itstep.j2019trainermanagerlapinskaya.entity.Comment;

import java.util.ArrayList;
import java.util.List;

public class CommentMapper {

    public List<CommentPreviewDto> mapToDtoList(List<Comment> entities) {
        List<CommentPreviewDto> dtos = new ArrayList<>();
        // 0. В цикле:
        for (Comment entity : entities) {
            // 1. Создать новый DTO
            CommentPreviewDto dto = new CommentPreviewDto();
            // 2. Перелить в него данные из ENTITY
            dto.setId(entity.getId());
            dto.setMessage(entity.getMessage());
            dto.setTrainerId(entity.getTrainer());
            dto.setAssessment(entity.getAssessment());
            dto.setCreateDate(entity.getCreateDate());
            dto.setUserName(entity.getUserName());
            dto.setUserLastName(entity.getUserLastName());
            dto.setUserEmail(entity.getUserEmail());
            // 3. Доавить полученный DTO в список
            dtos.add(dto);
        }
        return dtos;
    }


    public Appointment mapToEntity(AppointmentCreateDto createDto) {
        Appointment appointment = new Appointment();

        appointment.setClientName(createDto.getClientName());
        appointment.setClientLastName(createDto.getClientLastName());
        appointment.setClientEmail(createDto.getClientEmail());
        appointment.setArrivalTime(createDto.getArrivalTime());
        appointment.setNumberTel(createDto.getNumberTel());
        appointment.setTrainer(createDto.getTrainer());

        return appointment;
    }


    // updated_dto -> entity
    public Comment mapToEntity(CommentUpdateDto updateDto) {
        Comment entity = new Comment();
        entity.setId(updateDto.getId());
        entity.setMessage(updateDto.getMessage());

        return entity;
    }

    public CommentFullDto mapToDto(Comment entity) {

        CommentFullDto dto = new CommentFullDto();
        dto.setId(entity.getId());
        dto.setCreateDate(entity.getCreateDate());
        dto.setMessage(entity.getMessage());
        dto.setUserName(entity.getUserName());
        dto.setUserLastName(entity.getUserLastName());
        dto.setUserEmail(entity.getUserEmail());

        return dto;
    }
}
