package by.itstep.j2019trainermanagerlapinskaya.dto.appointment;

import lombok.Data;

@Data
public class AppointmentPreviewDto {

    private Long id;
    private String clientName;
    private String clientLastName;
    private String clientEmail;
    private String numberTel;   // номер телефона

}
