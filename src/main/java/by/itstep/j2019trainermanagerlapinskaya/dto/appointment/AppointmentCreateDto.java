package by.itstep.j2019trainermanagerlapinskaya.dto.appointment;

import by.itstep.j2019trainermanagerlapinskaya.entity.Trainer;
import lombok.Data;

import java.sql.Date;
@Data
public class AppointmentCreateDto {

    private String clientName;
    private String clientLastName;
    private String clientEmail;
    private String numberTel;
    private Trainer trainer;
    private Date arrivalTime;  // время прибытия



}
