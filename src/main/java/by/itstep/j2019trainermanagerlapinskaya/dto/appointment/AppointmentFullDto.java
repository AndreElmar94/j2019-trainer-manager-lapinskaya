package by.itstep.j2019trainermanagerlapinskaya.dto.appointment;

import by.itstep.j2019trainermanagerlapinskaya.entity.Trainer;
import lombok.Data;

import java.sql.Date;
@Data
public class AppointmentFullDto {

    // то что из entity перешло в готовую форму которую увидит пользователь
    private Long id;
    private String clientName;
    private String clientLastName;
    private String clientEmail;
    private String numberTel;   // номер телефона
    private Trainer trainer;
    private Date arrivalTime;  // время прибытия


}
