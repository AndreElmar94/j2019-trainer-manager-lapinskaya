package by.itstep.j2019trainermanagerlapinskaya.dto.trainer;
import by.itstep.j2019trainermanagerlapinskaya.entity.Comment;
import lombok.Data;

import java.util.List;
@Data
public class TrainerPreviewDto {

    private Long id;
    private String name;
    private String lastName;
    private String imageUrl; // аватарка
    private int workExperience; // стаж работы
    private String progress;    // достижения
    private List<Comment> comments;
}
