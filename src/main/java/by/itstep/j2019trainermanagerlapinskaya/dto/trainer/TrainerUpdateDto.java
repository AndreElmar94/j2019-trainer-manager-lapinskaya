package by.itstep.j2019trainermanagerlapinskaya.dto.trainer;

import lombok.Data;
@Data
public class TrainerUpdateDto {

    private Long id;
    private String name;
    private String lastName;
    private String password;    // хранится в БД
    private String email;
    private String imageUrl; // аватарка
    private int workExperience; // стаж работы
    private String progress;    // достижения

}
