package by.itstep.j2019trainermanagerlapinskaya.dto.admin;

import by.itstep.j2019trainermanagerlapinskaya.entity.Role;
import lombok.Data;

@Data
public class AdminPreviewDto {

    private Long id;
    private String name;
    private String lastName;
    private Role role;


}
