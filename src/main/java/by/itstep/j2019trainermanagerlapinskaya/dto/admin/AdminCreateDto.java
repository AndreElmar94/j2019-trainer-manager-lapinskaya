package by.itstep.j2019trainermanagerlapinskaya.dto.admin;

import lombok.Data;

@Data
public class AdminCreateDto {
    // форма с нужными полями для заполнения ПРИ создании

    private String name;
    private String lastName;
    private String email;
    private String password;

}
