package by.itstep.j2019trainermanagerlapinskaya.dto.admin;

import by.itstep.j2019trainermanagerlapinskaya.entity.Role;
import by.itstep.j2019trainermanagerlapinskaya.entity.Trainer;
import lombok.Data;

import java.util.List;
@Data
public class AdminFullDto {
    // ответ от Entity для DTO
    // готовая обработанная Entity форма , которую заполнил пользоватль изнаально в DTO
    // здесь поля ТЕ, котрые разрешил ENTITY показать пользователю

    private Long id;
    private String name;
    private String lastName;
    private String email;
    private String password;
    private Role role;
    private List<Trainer> tra;
}
