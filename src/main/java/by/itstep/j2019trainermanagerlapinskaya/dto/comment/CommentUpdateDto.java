package by.itstep.j2019trainermanagerlapinskaya.dto.comment;

import lombok.Data;
@Data
public class CommentUpdateDto {
    private Long id;
    private String message; // коммент к тренеру
}
