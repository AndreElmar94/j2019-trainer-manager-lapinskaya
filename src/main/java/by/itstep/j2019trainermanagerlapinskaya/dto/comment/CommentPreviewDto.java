package by.itstep.j2019trainermanagerlapinskaya.dto.comment;

import by.itstep.j2019trainermanagerlapinskaya.entity.Trainer;
import lombok.Data;

import java.sql.Date;

@Data
public class CommentPreviewDto {

    private Long id;
    private String message; // коммент к тренеру
    private Trainer trainerId; //под каким тренером коммент
    private String userName;
    private String userLastName;
    private String userEmail;
    private int assessment; // оценка для тренера в комменте (УСЛОВИЕ: от 1-10)
    private Date createDate; // дата создания коммента


}
