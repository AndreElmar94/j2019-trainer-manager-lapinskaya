package by.itstep.j2019trainermanagerlapinskaya.dto.comment;

import by.itstep.j2019trainermanagerlapinskaya.entity.Trainer;
import lombok.Data;
@Data
public class CommentCreateDto {

    private Long trainerId; //под каким тренером коммент
    private String message; // коммент к тренеру
    private int assessment; // оценка для тренера в комменте (УСЛОВИЕ: от 1-10)
    private String userName;
    private String userLastName;
    private String userEmail;
    private Long userId;


}
