package by.itstep.j2019trainermanagerlapinskaya.repository;

import by.itstep.j2019trainermanagerlapinskaya.entity.Comment;

import java.util.List;

public interface CommentRepository {
    //1. Найти всё
    List<Comment> findAll();
    //2. Найти по ID
    Comment findById(Long id);
    //3. Сохранить новый
    Comment create(Comment comment);
    //4. Обновить существующий
    Comment update(Comment comment);
    //5. Удалить по ID
    void deleteById(Long id);

    public void deleteAll();
}
