package by.itstep.j2019trainermanagerlapinskaya.repository;

import by.itstep.j2019trainermanagerlapinskaya.entity.Admin;

import java.util.List;

// Здесь делаем всё, что надо сделать через БД
public interface AdminRepository {
    //1. Найти всё
    List<Admin> findAll();
    //2. Найти по ID
    Admin findById(Long id);
    //3. Сохранить новый
    Admin create(Admin admin);
    //4. Обновить существующий
    Admin update(Admin admin);
    //5. Удалить по ID
    void deleteById(Long id);

     void deleteAll();
}
