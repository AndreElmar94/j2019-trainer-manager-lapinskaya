package by.itstep.j2019trainermanagerlapinskaya.repository.impl;

import by.itstep.j2019trainermanagerlapinskaya.entity.Comment;
import by.itstep.j2019trainermanagerlapinskaya.util.EntityManagerUtils;
import by.itstep.j2019trainermanagerlapinskaya.repository.CommentRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class CommentRepositoryImpl implements CommentRepository {
    @Override
    public List<Comment> findAll() {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        List<Comment> foundList = entityManager.createNativeQuery("SELECT * FROM by.itstep.j2019trainermanagerlapinskaya.dto.comment", Comment.class).getResultList();
        entityManager.close();
        System.out.println("Found " + foundList.size() + "by/itstep/j2019trainermanagerlapinskaya/dto/comment");
        return foundList;
    }

    @Override
    public Comment findById(Long id) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        Comment foundComment = entityManager.find(Comment.class, id);
        entityManager.close();
        System.out.println("Found by.itstep.j2019trainermanagerlapinskaya.dto.comment" + foundComment);
        return foundComment;
    }

    @Override
    public Comment create(Comment comment) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(comment);
        entityManager.getTransaction().commit();
        entityManager.close();
        System.out.println("Comment was created. Id: " + comment.getId());
        return comment;
    }

    @Override
    public Comment update(Comment comment) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(comment);


        entityManager.getTransaction().commit();
        entityManager.close();
        System.out.println("Comment was updated. Id: " + comment.getId());
        return comment;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();

        Comment foundComment = entityManager.find(Comment.class, id);
        entityManager.remove(foundComment);  // Удаление!

        entityManager.getTransaction().commit();    // Сохраняем всё что есть
        entityManager.close();
    }
    @Override
    public void deleteAll() {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin(); // стартуем транзакцию
        entityManager.createNativeQuery("DELETE FROM by.itstep.j2019trainermanagerlapinskaya.dto.comment").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
