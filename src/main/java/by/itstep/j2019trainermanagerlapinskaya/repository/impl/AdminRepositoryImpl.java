package by.itstep.j2019trainermanagerlapinskaya.repository.impl;

import by.itstep.j2019trainermanagerlapinskaya.entity.Admin;
import by.itstep.j2019trainermanagerlapinskaya.util.EntityManagerUtils;
import by.itstep.j2019trainermanagerlapinskaya.repository.AdminRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class AdminRepositoryImpl implements AdminRepository {
    @Override
    public List<Admin> findAll() {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        List<Admin> foundList = entityManager.createNativeQuery("SELECT * FROM by.itstep.j2019trainermanagerlapinskaya.dto.by.itstep.j2019trainermanagerlapinskaya.dto.admin", Admin.class).getResultList();
        entityManager.close();
        System.out.println("Found " + foundList.size() + "by/itstep/j2019trainermanagerlapinskaya/dto/by.itstep.j2019trainermanagerlapinskaya.dto.by.itstep.j2019trainermanagerlapinskaya.dto.admin");
        return foundList;
    }

    @Override
    public Admin findById(Long id) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        Admin foundAdmin = entityManager.find(Admin.class, id);
        entityManager.close();
        System.out.println("Found by.itstep.j2019trainermanagerlapinskaya.dto.by.itstep.j2019trainermanagerlapinskaya.dto.admin " + foundAdmin);
        return foundAdmin;
    }

    @Override
    public Admin create(Admin admin) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(admin);
        entityManager.getTransaction().commit();
        entityManager.close();
        System.out.println("Admin was created. Id: " + admin.getId());
        return admin;
    }

    @Override
    public Admin update(Admin admin) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(admin); // merge - заменит найденного адмимна (ИЗМЕНИТ)
        entityManager.getTransaction().commit();
        entityManager.close();
        System.out.println("Admin was updated. Id: " + admin.getId());
        return admin;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();

        Admin foundAdmin = entityManager.find(Admin.class, id);
        entityManager.remove(foundAdmin);  // Удаление!

        entityManager.getTransaction().commit();    // Сохраняем всё что есть
        entityManager.close();
    }

    @Override
    public void deleteAll() {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin(); // стартуем транзакцию
        entityManager.createNativeQuery("DELETE FROM by.itstep.j2019trainermanagerlapinskaya.dto.by.itstep.j2019trainermanagerlapinskaya.dto.admin").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
