package by.itstep.j2019trainermanagerlapinskaya.repository.impl;

import by.itstep.j2019trainermanagerlapinskaya.entity.Trainer;
import by.itstep.j2019trainermanagerlapinskaya.util.EntityManagerUtils;
import by.itstep.j2019trainermanagerlapinskaya.repository.TrainerRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class TrainerRepositoryImpl implements TrainerRepository {
    @Override
    public List<Trainer> findAll() {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        List<Trainer> foundList = entityManager.createNativeQuery("SELECT * FROM by.itstep.j2019trainermanagerlapinskaya.dto.trainer", Trainer.class).getResultList();
        entityManager.close();
        System.out.println("Found" + foundList.size() + "by/itstep/j2019trainermanagerlapinskaya/dto/trainer");
        return foundList;
    }

    @Override
    public Trainer findById(Long id) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        Trainer foundTrainer = entityManager.find(Trainer.class, id);
        entityManager.close();
        System.out.println("Found by.itstep.j2019trainermanagerlapinskaya.dto.trainer" + foundTrainer);
        return foundTrainer;
    }

    @Override
    public Trainer create(Trainer trainer) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(trainer);
        entityManager.getTransaction().commit();
        entityManager.close();
        System.out.println("Trainer was created. Id: " + trainer.getId());
        return trainer;
    }

    @Override
    public Trainer update(Trainer trainer) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(trainer);

        entityManager.getTransaction().commit();
        entityManager.close();
        System.out.println("Trainer was updated. Id: " + trainer.getId());
        return trainer;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();

        Trainer foundTrainer = entityManager.find(Trainer.class, id);
        entityManager.remove(foundTrainer);  // Удаление!

        entityManager.getTransaction().commit();    // Сохраняем всё что есть
        entityManager.close();
    }
    @Override
    public void deleteAll() {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin(); // стартуем транзакцию
        entityManager.createNativeQuery("DELETE FROM by.itstep.j2019trainermanagerlapinskaya.dto.trainer").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
