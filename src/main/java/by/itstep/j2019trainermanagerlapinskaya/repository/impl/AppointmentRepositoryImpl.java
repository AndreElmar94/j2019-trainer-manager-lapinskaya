package by.itstep.j2019trainermanagerlapinskaya.repository.impl;

import by.itstep.j2019trainermanagerlapinskaya.entity.Appointment;
import by.itstep.j2019trainermanagerlapinskaya.util.EntityManagerUtils;
import by.itstep.j2019trainermanagerlapinskaya.repository.AppointmentRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class AppointmentRepositoryImpl implements AppointmentRepository {
    @Override
    public List<Appointment> findAll() {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        List<Appointment> foundList = entityManager.createNamedQuery("SELECT * FROM by.itstep.j2019trainermanagerlapinskaya.dto.appointment", Appointment.class).getResultList();
        entityManager.close();
        System.out.println("Found" + foundList.size() + "by/itstep/j2019trainermanagerlapinskaya/dto/appointment");
        return foundList;
    }

    @Override
    public Appointment findById(Long id) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        Appointment foundAppointment = entityManager.find(Appointment.class, id);
        entityManager.close();
        System.out.println("Found by.itstep.j2019trainermanagerlapinskaya.dto.appointment " + foundAppointment);
        return foundAppointment;
    }

    @Override
    public Appointment create(Appointment appointment) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(appointment);
        entityManager.getTransaction().commit();
        entityManager.close();
        System.out.println("Appointment was created. Id: " + appointment.getId());
        return appointment;
    }

    @Override
    public Appointment update(Appointment appointment) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(appointment);


        entityManager.getTransaction().commit();
        entityManager.close();
        System.out.println("Appointment was updated. Id: " + appointment.getId());
        return appointment;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();
        Appointment foundAppointment = entityManager.find(Appointment.class, id);
        entityManager.remove(foundAppointment);  // Удаление!
        entityManager.getTransaction().commit();    // Сохраняем всё что есть
        entityManager.close();
    }

    @Override
    public void deleteAll() {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin(); // стартуем транзакцию
        entityManager.createNativeQuery("DELETE FROM by.itstep.j2019trainermanagerlapinskaya.dto.appointment").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
