package by.itstep.j2019trainermanagerlapinskaya.repository;

import by.itstep.j2019trainermanagerlapinskaya.entity.Appointment;

import java.util.List;

public interface AppointmentRepository {
    //1. Найти всё
    List<Appointment> findAll();
    //2. Найти по ID
    Appointment findById(Long id);
    //3. Сохранить новый
    Appointment create(Appointment appointment);
    //4. Обновить существующий
    Appointment update(Appointment appointment);
    //5. Удалить по ID
    void deleteById(Long id);

    void deleteAll();
}
