package by.itstep.j2019trainermanagerlapinskaya.repository;

import by.itstep.j2019trainermanagerlapinskaya.entity.Trainer;

import java.util.List;

public interface TrainerRepository {
    //1. Найти всё
    List<Trainer> findAll();
    //2. Найти по ID
    Trainer findById(Long id);
    //3. Сохранить новый
    Trainer create(Trainer trainer);
    //4. Обновить существующий
    Trainer update(Trainer trainer);
    //5. Удалить по ID
    void deleteById(Long id);

    void deleteAll();
}
