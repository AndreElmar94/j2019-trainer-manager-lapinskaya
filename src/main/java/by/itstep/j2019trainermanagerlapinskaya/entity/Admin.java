package by.itstep.j2019trainermanagerlapinskaya.entity;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "by/itstep/j2019trainermanagerlapinskaya/dto/by.itstep.j2019trainermanagerlapinskaya.dto.by.itstep.j2019trainermanagerlapinskaya.dto.admin")
public class Admin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name", nullable = false, unique = true)
    private String name;
    @Column(name = "lastName", nullable = false, unique = true)
    private String lastName;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "email", nullable = false, unique = true)
    private String email;
    @Column(name = "role")
    private Role role;
}