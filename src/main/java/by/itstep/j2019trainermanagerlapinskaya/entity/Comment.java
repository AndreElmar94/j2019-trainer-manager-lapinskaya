package by.itstep.j2019trainermanagerlapinskaya.entity;
import lombok.*;

import javax.persistence.*;
import java.sql.Date;
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "by/itstep/j2019trainermanagerlapinskaya/dto/comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "userName", nullable = false, unique = true)
    private String userName;
    @Column(name = "userLastName", nullable = false, unique = true)
    private String userLastName;
    @Column(name = "userEmail", nullable = false, unique = true)
    private String userEmail;
    @Column(name = "message")
    private String message; // коммент к тренеру
    @Column(name = "assessment")
    private int assessment; // оценка для тренера в комменте (УСЛОВИЕ: от 1-10)
    @Column(name = "date")
    @EqualsAndHashCode.Exclude
    private Date createDate; // дата создания коммента
    @Column(name = "published")
    private boolean published; //опубликован коммент или нет
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn(name = "trainer_id")
    private Trainer trainer; //под каким тренером коммент

}

// условие для оценки ! -> от 1 до 10