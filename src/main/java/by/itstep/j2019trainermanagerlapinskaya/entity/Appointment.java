package by.itstep.j2019trainermanagerlapinskaya.entity;
import lombok.*;

import javax.persistence.*;
import java.sql.Date;
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "by/itstep/j2019trainermanagerlapinskaya/dto/appointment")
public class Appointment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "clientName", nullable = false, unique = true)
    private String clientName;
    @Column(name = "clientLastName", nullable = false, unique = true)
    private String clientLastName;
    @Column(name = "clientEmail", nullable = false, unique = true)
    private String clientEmail;
    @Column(name = "date")
    @EqualsAndHashCode.Exclude
    private Date arrivalTime;  // время прибытия
    @Column(name = "numberTel")
    private String numberTel;   // номер телефона
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn(name = "trainer_id")
    private Trainer trainer;
}
