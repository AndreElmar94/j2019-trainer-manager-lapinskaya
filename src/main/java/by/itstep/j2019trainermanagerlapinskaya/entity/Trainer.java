package by.itstep.j2019trainermanagerlapinskaya.entity;
import lombok.*;

import javax.persistence.*;
import java.util.List;
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "by/itstep/j2019trainermanagerlapinskaya/dto/trainer")
public class Trainer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name", nullable = false, unique = true)
    private String name;
    @Column(name = "lastName", nullable = false, unique = true)
    private String lastName;
    @Column(name = "password", nullable = false)
    private String password;    // хранится в БД
    @Column(name = "email", nullable = false, unique = true)
    private String email;
    @Column(name = "imageUrl")
    private String imageUrl; // аватарка
    @Column(name = "workExperience")
    private int workExperience; // стаж работы
    @Column(name = "progress")
    private String progress;    // достижения
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "by/itstep/j2019trainermanagerlapinskaya/dto/trainer")
    private List<Appointment> appointments; // список записей клиентов к тренерам
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "by/itstep/j2019trainermanagerlapinskaya/dto/trainer")
    private List<Comment> comments;
}
