package by.itstep.j2019trainermanagerlapinskaya;
import by.itstep.j2019trainermanagerlapinskaya.entity.Appointment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import by.itstep.j2019trainermanagerlapinskaya.repository.AppointmentRepository;
import by.itstep.j2019trainermanagerlapinskaya.repository.AppointmentRepositoryImpl;
import java.sql.Date;
import java.util.List;
@SpringBootTest
class AppointmentTests {
    private AppointmentRepository appointmentRepository = new AppointmentRepositoryImpl();

    @BeforeEach
    void setUp() {  // каждый раз надо обновлять БД, чтобы не засорять её
        appointmentRepository.deleteAll();
    }
    @Test
    void testFindAll() {
        //given
        Appointment appointment = Appointment.builder()
                .clientName("Oleg")
                .clientLastName("Martinkevich")
                .clientEmail("olegMart@mail.ru")
                .arrivalTime(new Date(2000 - 12 - 12))
                .numberTel("+375441111111")
                .build();
        Appointment saved = appointmentRepository.create(appointment);
        //when
        List<Appointment> found = appointmentRepository.findAll();
        //then
        Assertions.assertEquals(1, found.size());
    }
    @Test
    void testFindById() {
        //given
        Appointment appointment = Appointment.builder()
                .clientName("Oleg")
                .clientLastName("Martinkevich")
                .clientEmail("olegMart@mail.ru")
                .arrivalTime(new Date(2000 - 12 - 12))
                .numberTel("+375441111111")
                .build();
        Appointment saved = appointmentRepository.create(appointment);
        //Long id = saved.getId();
        //when
        Appointment foundAppointment = appointmentRepository.findById(saved.getId());
        //then
        Assertions.assertNotNull(foundAppointment.getId());
        Assertions.assertEquals(saved, foundAppointment);
        //   Assertions.assertTrue(saved.equals(foundAppointment));
    }
    @Test
    void testCreate() {
        //given
        Appointment appointment = Appointment.builder()
                .clientName("Oleg")
                .clientLastName("Martinkevich")
                .clientEmail("olegMart@mail.ru")
                .arrivalTime(new Date(2000 - 12 - 12))
                .numberTel("+375441111111")
                .build();
        //when
        Appointment saved = appointmentRepository.create(appointment);
        //then
        Assertions.assertNotNull(saved.getId());    // убеждаемся, что ID не null
    }
    @Test
    void testUpdate() {
        //given
        Appointment appointment = Appointment.builder()
                .clientName("Oleg")
                .clientLastName("Martinkevich")
                .clientEmail("olegMart@mail.ru")
                .arrivalTime(new Date(2000 - 12 - 12))
                .numberTel("+375441111111")
                .build();
       appointmentRepository.create(appointment);
        Appointment appointment1 = Appointment.builder().id(appointment.getId())
                .clientName("Misha")
                .clientLastName("Ololo")
                .clientEmail("olololo@mail.ru")
                .arrivalTime(new Date(2000 - 12 - 12))
                .numberTel("+375441111111")
                .build();
        //when
        Appointment savedAppointment = appointmentRepository.update(appointment1);
        //then
        Assertions.assertEquals("Misha", savedAppointment.getClientName());
        Assertions.assertEquals("Ololo", savedAppointment.getClientLastName());
        Assertions.assertEquals("olololo@mail.ru", savedAppointment.getClientEmail());
    }
    @Test
    void testDeleteById() {
        //given
        Appointment appointment = Appointment.builder()
                .clientName("Oleg")
                .clientLastName("Martinkevich")
                .clientEmail("olegMart@mail.ru")
                .arrivalTime(new Date(2000 - 12 - 12))
                .numberTel("+375441111111")
                .build();
        Appointment savedAppointment = appointmentRepository.create(appointment);   // добваляю в базу и сохраняю там ID его
        //when
        appointmentRepository.deleteById(savedAppointment.getId()); // удалила из базы данных
        //then
        Assertions.assertNull(appointmentRepository.findById(savedAppointment.getId()));    // убеждаюмь что id у этого человека NULL что он действительно удалился из базы
    }
}
