package by.itstep.j2019trainermanagerlapinskaya;

import by.itstep.j2019trainermanagerlapinskaya.entity.Admin;
import by.itstep.j2019trainermanagerlapinskaya.entity.Role;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import by.itstep.j2019trainermanagerlapinskaya.repository.AdminRepository;
import by.itstep.j2019trainermanagerlapinskaya.repository.AdminRepositoryImpl;

import java.util.List;

@SpringBootTest
public class AdminTests {
    private AdminRepository adminRepository = new AdminRepositoryImpl();

    @BeforeEach
    void setUp() {  // каждый раз надо обновлять БД, чтобы не засорять её
        adminRepository.deleteAll();
    }

    @Test
    void testFindAll() {
        //given
        Admin admin = Admin.builder()
                .name("Anna")
                .lastName("Baranova")
                .password("123")
                .email("annaBar")
                .role(Role.ADMIN)
                .build();
        Admin savedAdmin = adminRepository.create(admin);
        //when
        List<Admin> found = adminRepository.findAll();
        //then
        Assertions.assertEquals(1, found.size());
    }

    @Test
    void testFindById() {
        //given
        Admin admin = Admin.builder()
                .name("Anna")
                .lastName("Baranova")
                .password("123")
                .email("annaBar")
                .role(Role.ADMIN)
                .build();
        Admin savedAdmin = adminRepository.create(admin);
        // Long id = savedAdmin.getId();
        //when
        Admin found = adminRepository.findById(savedAdmin.getId());
        //then
        Assertions.assertNotNull(found.getId());
        Assertions.assertEquals(savedAdmin, found);
        // Assertions.assertTrue(savedAdmin.equals(found));
    }

    @Test
    void testCreate() {
        //given
        Admin admin = Admin.builder()
                .name("Anna")
                .lastName("Baranova")
                .password("123")
                .email("annaBar")
                .role(Role.ADMIN)
                .build();
        //when
        Admin savedAdmin = adminRepository.create(admin);
        //then
        Assertions.assertNotNull(savedAdmin.getId());   // убеждаемся, что ID не null
    }

    @Test
    void testUpdate() {
        //given
        Admin admin = Admin.builder()
                .name("Anna")
                .lastName("Baranova")
                .password("123")
                .email("annaBar")
                .role(Role.ADMIN)
                .build();
        adminRepository.create(admin);
        Admin admin2 = Admin.builder().id(admin.getId())
                .name("Kate")
                .lastName("Makarova")
                .password("123")
                .email("katM")
                .role(Role.ADMIN)
                .build();
        //when
        Admin savedAdmin = adminRepository.update(admin2);
        //then
        Assertions.assertEquals("Kate", savedAdmin.getName());
        Assertions.assertEquals("Makarova", savedAdmin.getLastName());
        Assertions.assertEquals("katM", savedAdmin.getEmail());
    }

    @Test
    void testDeleteById() {
        //given
        Admin admin = Admin.builder()
                .name("Anna")
                .lastName("Baranova")
                .password("123")
                .email("annaBar")
                .role(Role.ADMIN)
                .build();
        Admin savedAdmin = adminRepository.create(admin); // добваляю в базу и сохраняю там ID его
        //when
        adminRepository.deleteById(savedAdmin.getId()); // удалила его из базы
        //then
        Assertions.assertNull(adminRepository.findById(savedAdmin.getId()));   // убеждаюмь что id у этого человека NULL что он действительно удалился из базы
    }
}
