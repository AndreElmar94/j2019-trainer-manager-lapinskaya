package by.itstep.j2019trainermanagerlapinskaya;
import by.itstep.j2019trainermanagerlapinskaya.entity.Comment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import by.itstep.j2019trainermanagerlapinskaya.repository.CommentRepository;
import by.itstep.j2019trainermanagerlapinskaya.repository.CommentRepositoryImpl;
import java.sql.Date;
import java.util.List;

@SpringBootTest
public class CommentTest {
    private CommentRepository commentRepository = new CommentRepositoryImpl();

    // обновляем каждый раз БД, чтоб не засорять её
    @BeforeEach
    void setUp() {
        commentRepository.deleteAll();
    }

    @Test
    void testFindAll() {
        //given
        Comment comment = Comment.builder()
                .userName("Vanya")
                .userLastName("Oparko")
                .message("Ololo")
                .userEmail("aasa")
                .assessment(9)
                .createDate(new Date(2000 - 12 - 12))
                .published(true)
                .build();
        Comment savedComment = commentRepository.create(comment);
        //when
        List<Comment> found = commentRepository.findAll();
        //then
        Assertions.assertEquals(1, found.size());
    }

    @Test
    void testFindById() {
        //given
        Comment comment = Comment.builder()
                .userName("Vanya")
                .userLastName("Oparko")
                .userEmail("aasa")
                .message("Ololo")
                .assessment(9)
                .createDate(new Date(2000 - 12 - 12))
                .published(true)
                .build();
        Comment saved = commentRepository.create(comment);
        //Long id = saved.getId();
        //when
        Comment foundComment = commentRepository.findById(saved.getId());
        //then
        Assertions.assertNotNull(foundComment.getId());
        System.out.println(saved.equals(foundComment));
        Assertions.assertEquals(saved, foundComment);
        //   Assertions.assertTrue(saved.equals(foundComment));
    }

    @Test
    void testCreate() {
        //given
        Comment comment = Comment.builder()
                .userName("Vanya")
                .userLastName("Oparko")
                .userEmail("aasa")
                .message("Ololo")
                .assessment(9)
                .createDate(new Date(2000 - 12 - 12))
                .published(true)
                .build();
        //when
        Comment saved = commentRepository.create(comment);
        //then
        Assertions.assertNotNull(saved.getId());    // убеждаемся, что ID не null
    }

    @Test
    void testUpdate() {
        //given
        Comment comment = Comment.builder()
                .userName("Vanya")
                .userLastName("Oparko")
                .userEmail("aasa")
                .message("Ololo")
                .assessment(9)
                .createDate(new Date(2000 - 12 - 12))
                .published(true)
                .build();
        commentRepository.create(comment);
        Comment comment1 = Comment.builder().id(comment.getId())
                .userName("Vanya")
                .userLastName("Oparko")
                .userEmail("aasa")
                .message("Bababa")
                .assessment(9)
                .createDate(new Date(2000 - 12 - 12))
                .published(true)
                .build();
        //when
        Comment savedComment = commentRepository.update(comment1);
        //then
        Assertions.assertEquals("Vanya", savedComment.getUserName());
        Assertions.assertEquals("Oparko", savedComment.getUserLastName());
        Assertions.assertEquals("Bababa", savedComment.getMessage());
    }

    @Test
    void testDeleteById() {
        //given
        Comment comment = Comment.builder()
                .userName("Vanya")
                .userLastName("Oparko")
                .userEmail("aasa")
                .message("Ololo")
                .assessment(9)
                .createDate(new Date(2000 - 12 - 12))
                .published(true)
                .build();
        Comment savedComment = commentRepository.create(comment);   // добваляю в базу КОММЕНТ и сохраняю там ID его
        //when
        commentRepository.deleteById(savedComment.getId()); // удалила из базы данных
        //then
        Assertions.assertNull(commentRepository.findById(savedComment.getId()));    // убеждаюмь что id у этого комменрта NULL что он действительно удалился из базы
    }
}
