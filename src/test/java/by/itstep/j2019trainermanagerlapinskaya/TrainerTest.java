package by.itstep.j2019trainermanagerlapinskaya;
import by.itstep.j2019trainermanagerlapinskaya.entity.Trainer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import by.itstep.j2019trainermanagerlapinskaya.repository.TrainerRepository;
import by.itstep.j2019trainermanagerlapinskaya.repository.TrainerRepositoryImpl;
import java.util.List;
public class TrainerTest {
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    // обновляем каждый раз БД, чтоб не засорять её
    @BeforeEach
    void setUp() {
        trainerRepository.deleteAll();
    }

    @Test
    void testFindAll() {
        //given
        Trainer trainer = Trainer.builder()
                .name("Bo")
                .lastName("Pa")
                .password("Ololo")
                .email("aasa")
                .imageUrl("cat.jpg")
                .workExperience(10) // стаж работы
                .progress("Dostizhenia")
                .build();
        Trainer savedTrainer = trainerRepository.create(trainer);
        //when
        List<Trainer> found = trainerRepository.findAll();
        //then
        Assertions.assertEquals(1, found.size());
    }

    @Test
    void testFindById() {
        //given
        Trainer trainer = Trainer.builder()
                .name("Bo")
                .lastName("Pa")
                .password("Ololo")
                .email("aasa")
                .imageUrl("cat.jpg")
                .workExperience(10) // стаж работы
                .progress("Dostizhenia")
                .build();
        Trainer saved = trainerRepository.create(trainer);
        //Long id = saved.getId();
        //when
        Trainer foundTrainer = trainerRepository.findById(saved.getId());
        //then
        Assertions.assertNotNull(foundTrainer.getId());
        System.out.println(saved.equals(foundTrainer));
        Assertions.assertEquals(saved, foundTrainer);
        //   Assertions.assertTrue(saved.equals(foundTrainer));
    }

    @Test
    void testCreate() {
        //given
        Trainer trainer = Trainer.builder()
                .name("Bo")
                .lastName("Pa")
                .password("Ololo")
                .email("aasa")
                .imageUrl("cat.jpg")
                .workExperience(10) // стаж работы
                .progress("Dostizhenia")
                .build();
        //when
        Trainer saved = trainerRepository.create(trainer);
        //then
        Assertions.assertNotNull(saved.getId());    // убеждаемся, что ID не null
    }

    @Test
    void testUpdate() {
        //given
        Trainer trainer = Trainer.builder()
                .name("Bo")
                .lastName("Pa")
                .password("Ololo")
                .email("aasa")
                .imageUrl("cat.jpg")
                .workExperience(10) // стаж работы
                .progress("Dostizhenia")
                .build();
        trainerRepository.create(trainer);
        Trainer trainer1 = Trainer.builder().id(trainer.getId())
                .name("Giii")
                .lastName("Poooo")
                .password("Ololo")
                .email("aasa")
                .imageUrl("cat.jpg")
                .workExperience(10) // стаж работы
                .progress("Dostizhenia")
                .build();
        //when
        Trainer savedTrainer= trainerRepository.update(trainer1);
        //then
        Assertions.assertEquals("Giii", savedTrainer.getName());
        Assertions.assertEquals("Poooo", savedTrainer.getLastName());
        Assertions.assertEquals("Ololo", savedTrainer.getPassword());
    }

    @Test
    void testDeleteById() {
        //given
        Trainer trainer = Trainer.builder()
                .name("Bo")
                .lastName("Pa")
                .password("Ololo")
                .email("aasa")
                .imageUrl("cat.jpg")
                .workExperience(10) // стаж работы
                .progress("Dostizhenia")
                .build();
        Trainer savedTrainer = trainerRepository.create(trainer);   // добваляю в базу тренера и сохраняю там ID его
        //when
        trainerRepository.deleteById(savedTrainer.getId()); // удалила из базы данных
        //then
        Assertions.assertNull(trainerRepository.findById(savedTrainer.getId()));    // убеждаюмь что id у этого тренера NULL что он действительно удалился из базы
    }

}
